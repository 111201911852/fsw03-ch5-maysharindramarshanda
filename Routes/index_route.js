const express = require('express');
const router = express.Router();

const carsRoute = require('./cars_route');


router.get('/', (req, res) => {
    res.render('index', {title: 'Halaman Menu'});
});
router.use('/cars', carsRoute);

module.exports = router;