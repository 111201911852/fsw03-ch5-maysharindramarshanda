const express = require('express');
const Controller = require('../controllers/cars_controller');
const router = express.Router();

//menampilkan .
router.get('/', (req, res) => {
    Controller.getAll(req, res);
})
router.get('/addnewcar', (req,res) => {
    res.render('addnewcar');
})
//create .
router.post('/create', (req, res) => {
    Controller.create(req, res);
})

//menampilkan update
router.post('/edit/:id', (req, res) => {
    Controller.update(req, res);
})

//menampilkan id .
router.get('/:id', (req, res) => {
    Controller.getById(req, res);
})

//menampilkan add
// router.post('/edit/:id', (req, res) => {
//     res.render('editcar');
// })
router.get('/edit/:id', (req, res) => {
    Controller.formUpdate(req, res);
})
//merubah update
router.get('/edit/:id', (req, res) => {
    Controller.update(req, res);
})
//delete
router.get('/delete/:id', (req, res) => {
    Controller.delete(req, res);
})

module.exports = router;