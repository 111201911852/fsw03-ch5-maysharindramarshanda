//Cars dari model cars
const { Cars } = require('../models/index');


class Controller {
    static getAll(req, res) {
        Cars.findAll()
            .then(car => {
                res.render('car',{car});
            })
            .catch(err => {
                res.send(err);
            })
    }

    static create(req, res) {
        Cars.create(req.body)
            .then(cars => {
                res.redirect('/cars');
            })
            .catch(err => {
                res.send(err);
            })
    }

    static getById(req, res) {
        Cars.findByPk(req.params.id)
            .then(cars => {
                res.status(200).json(cars);
            })
            .catch(err => {
                res.send(err);
            })
    }

    static formUpdate(req, res) {
        Cars.findByPk(req.params.id)
            .then(cars => {
                res.render('editcar', {cars});
            })
            .catch(err => {
                res.send(err);
            })
    }

    static update(req, res) {
        Cars.update(req.body, {
            where: { 
                id: req.params.id
            }
        })
            .then(cars => {
                res.redirect('/cars');
            })
            .catch(err => {
                res.send(err);
            })
    }

    static delete(req, res) {
        Cars.destroy({
            where: {
                id: req.params.id
            }
        })
            .then(cars => {
                res.redirect('/cars');
            })
            .catch(err => {
                res.send(err);
            })
    }
}

module.exports = Controller;
