const express = require('express');
const routes = require('./routes/index_route');

const app = express();
const port = 3000;

app.set('view engine', 'ejs');
app.use(express.static('asset/css'));
app.use(express.static('asset/js'));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(routes);

app.listen(port, () => {
    console.log(`Example app listening on port ${port}!`);
});